import math


def input_check(string: str) -> tuple:
    """
    Проверяет поданую строку на наличие символов '0' и '1'.

    Принимает на вход строку. Возвращает кортеж, в котором на первом 
    месте идет количество символов в строке, а на втором месте идет 
    количество символов '0'.

    Args:
        string:    Строка состоящая из символов '0' и '1'.

    Returns:
        Кортеж, в котором на первом месте идет количество символов в 
        строке, а на втором месте идет количество символов '0'.

    Raises:
        ValueError.

    Examples:
        >>> input_check("01")
        (2, 1)
        >>> input_check("aaa")
        Traceback (most recent call last):
            ...
        ValueError: Wrong input
        >>> input_check("012")
        Traceback (most recent call last):
            ...
        ValueError: Wrong digit in input
    """
    if not string.isdigit():
        raise ValueError("Wrong input")

    bad_digits = set("23456789")
    if bad_digits.intersection(set(string)):
        raise ValueError("Wrong digit in input")

    return (len(string), string.count("0"))


def binomial_coefficient(n: int, k: int) -> int:
    """
    Вычисляет число сочетаний из n элементов по k элементов.

    Принимает на вход два натуральных либо равных нулю числа n, k.
    Число n должно быть больше или равно числу k. Возвращает число 
    возможных сочетаний.

    Args:
        n:    Количество элементов исходного множества.
        k:    Количество элементов, выбираемое из исходного множства.

    Returns:
        Число сочетаний из n элементов по k элементов.

    Raises:
        ValueError.

    Examples:
        >>> binomial_coefficient(5, 2)
        10
        >>> binomial_coefficient(2, 5)
        Traceback (most recent call last):
            ...
        ValueError: factorial() not defined for negative values
        >>> binomial_coefficient(0.5, 2)
        Traceback (most recent call last):
            ...
        ValueError: factorial() only accepts integral values
    """
    return math.factorial(n) // math.factorial(n - k) // math.factorial(k)


def main():
    """
    Функция, осуществляющая работу программы в консоли.
    """
    while True:
        try:
            input_tuple = input_check(input("Enter sequence of 0 and 1: "))
        except ValueError as error:
            print(error)
            continue
        break
    print("Result: ", binomial_coefficient(*input_tuple))


if __name__ == "__main__":
    main()
