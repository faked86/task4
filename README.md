# Практическая работа №5

**Цели:**

* изучить принципы составления программной документации;
* научиться оформлять README.md файл на удаленном репозитории;
* научиться писать документацию для Doxygen.

*Алгоритм решения поставленной задачи:*

1. Написать документацию для Doxygen для всех функций в своей предыдущей практической работе.
2. Написать документацию для модульных тестов.
3. Сгенерировать документацию кода с помощью Doxygen и получить html страничку документации.
4. Залить документацию на удаленный репозиторий.
5. Оформить файл README.md на удаленном репозитории в gitlab.

Программа реализована в соответствии со следующим заданием:


> Имеется некоторая последовательность бит BIN, состоящая из хаотично размещённых нулей и единиц. Необходимо найти количество последовательностей, состоящих из того же числа нулей и единиц что и BIN.

***Код функции main:***

```
while True:
    try:
        input_tuple = input_check(input("Enter sequence of 0 and 1: "))
    except ValueError as error:
        print(error)
        continue
    break
print("Result: ", binomial_coefficient(*input_tuple))
```

Программа использует [формулу числа сочетаний](https://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D1%87%D0%B5%D1%82%D0%B0%D0%BD%D0%B8%D0%B5):

$`{n \choose k}=C_{n}^{k}={\frac {n!}{k!\left(n-k\right)!}}`$

Изображение, демонстрирующее пример нахождения количества сочетаний из 5 элементов по 3:

![Сочетания](https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Combinations_without_repetition%3B_5_choose_3.svg/1024px-Combinations_without_repetition%3B_5_choose_3.svg.png)