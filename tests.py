import pytest
import main


"""Тесты для input_check:"""


def test_input_check_normal_cases():
    """Обычные случаи."""
    assert main.input_check("0") == (1, 1)
    assert main.input_check("1") == (1, 0)
    assert main.input_check("01") == (2, 1)


def test_input_check_empty_string():
    """Случай с пустой строкой."""
    with pytest.raises(ValueError, match="Wrong input"):
        main.input_check("")


def test_input_check_non_digits():
    """Случаи, в которых строка содержит символы кроме цифр."""
    with pytest.raises(ValueError, match="Wrong input"):
        main.input_check("aaa")
    with pytest.raises(ValueError, match="Wrong input"):
        main.input_check("1111a")
    with pytest.raises(ValueError, match="Wrong input"):
        main.input_check("@")


def test_input_check_wrong_digits():
    """Случаи, в которых строка содержит цифры кроме '0' и '1'."""
    with pytest.raises(ValueError, match="Wrong digit in input"):
        main.input_check("012")
    with pytest.raises(ValueError, match="Wrong digit in input"):
        main.input_check("0101103")


"""Тесты для binominal_coefficient:"""


def test_binominal_coefficient_normal_cases():
    """Обычные случаи."""
    assert main.binomial_coefficient(1, 0) == 1
    assert main.binomial_coefficient(5, 2) == 10


def test_binominal_coefficient_n_less_than_k():
    """Случаи, когда n < k."""
    with pytest.raises(ValueError):
        main.binomial_coefficient(0, 1)
    with pytest.raises(ValueError):
        main.binomial_coefficient(2, 5)


def test_binominal_coefficient_less_than_zero():
    """Случаи, когда n и/или k меньше нуля."""
    with pytest.raises(ValueError):
        main.binomial_coefficient(-1, 0)
    with pytest.raises(ValueError):
        main.binomial_coefficient(0, -1)
    with pytest.raises(ValueError):
        main.binomial_coefficient(-1, -1)


def test_binominal_coefficient_non_integral():
    """Случаи, когда n и/или k не являются целыми числами."""
    with pytest.raises(ValueError):
        main.binomial_coefficient(0.5, 0)
    with pytest.raises(ValueError):
        main.binomial_coefficient(5, 0.5)
    with pytest.raises(ValueError):
        main.binomial_coefficient(0.5, 0.5)
